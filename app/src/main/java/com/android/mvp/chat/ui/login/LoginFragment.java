package com.android.mvp.chat.ui.login;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.mvp.chat.R;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.home.HomeFragment;
import com.android.mvp.chat.ui.register.RegisterFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment implements LoginMvpView {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.et_user_login)
    EditText etUserLogin;


    @BindView(R.id.et_pass_login)
    EditText etPassLogin;

    @BindView(R.id.tv_register)
    TextView tvRegister;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);


        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        checkEditText();
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .add(R.id.frameContainer, RegisterFragment.newInstance(), RegisterFragment.TAG)
                            .addToBackStack(RegisterFragment.TAG)
                            .commit();
                }
            }
        });
    }

    @OnClick(R.id.btn_login)
    public void onLogin() {
        String user = etUserLogin.getText().toString().trim();
        Log.d("user", user);
        String pass = etPassLogin.getText().toString().trim();
        Log.d("pass", pass);
        if (user.isEmpty() || pass.isEmpty()) {
            Toast.makeText(getActivity(), getContext().getString(R.string.dont_input), Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.onLogin(user, pass);
        }
    }

    private void checkEditText() {
        etUserLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etUserLogin.getText().length() > 0 && etPassLogin.getText().length() > 0) {
                    btnLogin.setBackgroundResource(R.drawable.button_custom_blue);
                } else {
                    btnLogin.setBackgroundResource(R.drawable.button_custom);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPassLogin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etUserLogin.getText().length() > 0 && etPassLogin.getText().length() > 0) {
                    btnLogin.setBackgroundResource(R.drawable.button_custom_blue);
                } else {
                    btnLogin.setBackgroundResource(R.drawable.button_custom);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void openHomeFragment() {
        if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void onError() {
        Toast.makeText(getActivity(), getContext().getString(R.string.dont_input), Toast.LENGTH_SHORT).show();
    }
}
