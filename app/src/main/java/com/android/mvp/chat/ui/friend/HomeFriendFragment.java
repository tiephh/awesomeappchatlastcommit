package com.android.mvp.chat.ui.friend;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.request.FriendRequestFragment;
import com.android.mvp.chat.ui.friend.tabFriend.TabFriendFragment;
import com.android.mvp.chat.ui.friend.tabUser.TabUserFragment;
import com.android.mvp.chat.ui.login.LoginFragment;
import com.android.mvp.chat.ui.login.LoginMvpPresenter;
import com.android.mvp.chat.ui.login.LoginMvpView;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFriendFragment extends BaseFragment implements HomeFriendMvpView {

    public static final String TAG = HomeFriendFragment.class.getSimpleName();

    @BindView(R.id.tab_layout_friend)
    TabLayout tabLayoutFriend;

    @BindView(R.id.view_pager_friend)
    ViewPager viewPagerFriend;

    @BindView(R.id.et_search)
    EditText svSearch;

    @BindView(R.id.tv_request_number)
    TextView tvRequestNumber;


    @Inject
    HomFriendMvpPresenter<HomeFriendMvpView> mPresenter;

    public static HomeFriendFragment newInstance() {
        Bundle args = new Bundle();
        HomeFriendFragment fragment = new HomeFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public OnTextChanged onTextChanged;

    @Override
    public void readRequest(ArrayList<Users> userArrayList) {
        if (userArrayList.size() == 0) {
            tvRequestNumber.setVisibility(View.GONE);
        } else {
            tvRequestNumber.setVisibility(View.VISIBLE);
            tvRequestNumber.setText(String.valueOf(userArrayList.size()));
        }
    }

    public interface OnTextChanged {
        void onTextChanged(String word);
    }

    public void setOnTextChanged(OnTextChanged onTextChanged) {
        this.onTextChanged = onTextChanged;
    }

    public interface OnTextChangedUser {
        void onTextChangedUser(String word);
    }

    public OnTextChangedUser onTextChangedUser;

    public void setOnTextChangedUser(OnTextChangedUser onTextChangedUser) {
        this.onTextChangedUser = onTextChangedUser;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_friend, container, false);


        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }


    @Override
    protected void setUp(View view) {
        mPresenter.readRequest();

        svSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(viewPagerFriend.getCurrentItem() == 0){
                    if (onTextChanged != null) {
                        onTextChanged.onTextChanged(charSequence.toString());
                    }
                }else if(viewPagerFriend.getCurrentItem() == 1){
                    if (onTextChangedUser != null) {
                        onTextChangedUser.onTextChangedUser(charSequence.toString());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new TabFriendFragment(), getContext().getString(R.string.friend));
        adapter.addFragment(new TabUserFragment(), getContext().getString(R.string.tab_all));
        adapter.addFragment(new FriendRequestFragment(), getContext().getString(R.string.tab_request));
        viewPagerFriend.setAdapter(adapter);
        tabLayoutFriend.setupWithViewPager(viewPagerFriend);


    }
}
