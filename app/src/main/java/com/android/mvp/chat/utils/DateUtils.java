package com.android.mvp.chat.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private Date date;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    SimpleDateFormat monthF = new SimpleDateFormat("MM");
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
    SimpleDateFormat hour = new SimpleDateFormat("HH:mm");
    SimpleDateFormat day = new SimpleDateFormat("dd-MM-yyyy");

    public DateUtils(Date date) {
        this.date = date;
    }

    public String getMonth(){
        return monthF.format(date);
    }
    public String getYear(){
        return day.format(date);
    }
    public String getDate(){
        return dateFormat.format(date);
    }
    public String getHour(){
        return hour.format(date);
    }
}
