package com.android.mvp.chat.ui.register;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.android.mvp.chat.R;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.home.HomeFragment;
import com.android.mvp.chat.ui.login.LoginFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterFragment extends BaseFragment implements RegisterMvpView {

    public static final String TAG = RegisterFragment.class.getSimpleName();


    @Inject
    RegisterMvpPresenter<RegisterMvpView> mPresenter;


    public static RegisterFragment newInstance() {
        Bundle args = new Bundle();
        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.et_uername_res)
    EditText etUserRes;

    @BindView(R.id.et_email_res)
    EditText etEmailRes;

    @BindView(R.id.et_pass_res)
    EditText etPassRes;

    @BindView(R.id.cb_policy)
    AppCompatCheckBox cbPolicy;

    @BindView(R.id.btn_register)
    AppCompatButton btnRegister;

    @BindView(R.id.tv_login_now)
    TextView tvLoginNow;

    @BindView(R.id.btn_back_login)
    ImageButton btnBackLogin;

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        backLogin();
        checkEditText();
        registerClick(cbPolicy.isChecked());
        checkCheckBox();
    }

    private void checkCheckBox(){
        cbPolicy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(etEmailRes.getText().length() > 0 && etPassRes.getText().length() > 0 && etUserRes.getText().length() > 0 && cbPolicy.isChecked()){
                    registerClick(isChecked);
                    btnRegister.setBackgroundResource(R.drawable.button_custom_blue);
                }else{
                    btnRegister.setBackgroundResource(R.drawable.button_custom);
                }
            }
        });
    }

    private void registerClick(boolean isCheck){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = etUserRes.getText().toString().trim();
                String email = etEmailRes.getText().toString().trim();
                String pass = etPassRes.getText().toString().trim();
                mPresenter.onRegister(userName, email, pass, isCheck);
            }
        });
    }
    private void backLogin(){
        tvLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        btnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
    }


    private void checkEditText(){
        etUserRes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etEmailRes.getText().length() > 0 && etPassRes.getText().length() > 0 && etUserRes.getText().length() > 0 && cbPolicy.isChecked()){
                    btnRegister.setBackgroundResource(R.drawable.button_custom_blue);
                    registerClick(cbPolicy.isChecked());
                }else {
                    btnRegister.setBackgroundResource(R.drawable.button_custom);
                }
            }
        });
        etEmailRes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etEmailRes.getText().length() > 0 && etPassRes.getText().length() > 0 && etUserRes.getText().length() > 0 && cbPolicy.isChecked()){
                    btnRegister.setBackgroundResource(R.drawable.button_custom_blue);
                    registerClick(cbPolicy.isChecked());
                }else {
                    btnRegister.setBackgroundResource(R.drawable.button_custom);
                }
            }
        });
        etPassRes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etEmailRes.getText().length() > 0 && etPassRes.getText().length() > 0 && etUserRes.getText().length() > 0 && cbPolicy.isChecked()){
                    btnRegister.setBackgroundResource(R.drawable.button_custom_blue);
                    registerClick(cbPolicy.isChecked());
                }else {
                    btnRegister.setBackgroundResource(R.drawable.button_custom);
                }
            }
        });
    }



    @Override
    public void onSuccess() {
        mPresenter.onLogin(etEmailRes.getText().toString().trim(), etPassRes.getText().toString().trim());
        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
        etUserRes.setText("");
        etPassRes.setText("");
        etEmailRes.setText("");
        cbPolicy.setChecked(false);
        btnRegister.setBackgroundResource(R.drawable.button_custom);
    }

    @Override
    public void onFail() {
        Toast.makeText(getActivity(), "Fail", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void failPassWord() {
        Toast.makeText(getActivity(), "PassWord chưa đủ 6 kí tự", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void failInputData() {
        Toast.makeText(getActivity(), "Bạn chưa nhập thông tin", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void failPolicy() {
        Toast.makeText(getActivity(), "Bạn chưa đồng ý với điều khoản", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openHomeFragment() {
        if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                    .commit();
        }
    }
}
