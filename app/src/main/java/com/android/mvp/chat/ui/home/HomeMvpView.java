package com.android.mvp.chat.ui.home;

import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeMvpView extends MvpView {
    void readRooms(int count);
    void readRequest(ArrayList<Users> users);
}
