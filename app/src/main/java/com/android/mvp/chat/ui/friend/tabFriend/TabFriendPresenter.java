package com.android.mvp.chat.ui.friend.tabFriend;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.ui.friend.tabUser.UserSort;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TabFriendPresenter<V extends TabFriendMvpView> extends BasePresenter<V>
        implements TabFriendMvpPresenter<V> {


    @Inject
    public TabFriendPresenter(DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
    ArrayList<Users> users;
    ArrayList<SortUser> userSorts;

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getDataManager().getDatabaseReference(AppConstants.FRIENDS)
                .child(getDataManager().getFirebaseUserId()).keepSynced(true);
        getDataManager().getListUser().keepSynced(true);
    }

    @Override
    public void readUser() {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userArrayList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Users user = snapshot.getValue(Users.class);
                    if (user != null && getDataManager().getFirebaseUser()!=null) {
                        if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                            userArrayList.add(user);
                        }
                    }
                }
                readUserFriend(userArrayList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void readUserFriend(ArrayList<Users> userArrayList){
        users = new ArrayList<>();
        getDataManager().getListUser().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Users user = snapshot.getValue(Users.class);
                    for(int i = 0; i < userArrayList.size(); i++){
                        if (user != null && user.getId().equals(userArrayList.get(i).getId())) {
                            users.add(user);
                        }
                    }
                }
                sortListUserWithAlphabet(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void sortListUserWithAlphabet(ArrayList<Users> users) {
        users.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUser();
    }

    private void filterListUser() {
        userSorts = new ArrayList<>();
        String lastHeader = "";
        for (Users user : users) {
            String header = String.valueOf(user.getUsername().charAt(0)).toUpperCase();
            if (!header.equals(lastHeader)) {
                lastHeader = header;
                userSorts.add(new SortUser(header, null, true));
            }
            userSorts.add(new SortUser(header, user, false));
        }
        getMvpView().onSuccess(userSorts);
    }

    @Override
    public void checkExistsRoom(Users receiver) {
        getDataManager().getListRoomByUserOnceTime()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        for (DataSnapshot snapshot : task.getResult().getChildren()) {
                            Room room = snapshot.getValue(Room.class);
                            if (room != null && room.getReceiver() != null
                                    && room.getReceiver().equals(receiver.getId())) {
                                getMvpView().openChatRoom(room);
                                return;
                            }
                        }
                        createChatRoom(receiver);
                    } else if (task.getException() != null) {
                        getMvpView().showMessage(task.getException().getMessage());
                    }
                });
    }

    @Override
    public void searchUser(String word) {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        userArrayList.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Users user = dataSnapshot.getValue(Users.class);
                            assert user != null;
                            if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                userArrayList.add(user);
                            }
                        }
                        readUserSearch(userArrayList, word);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
    private void readUserSearch(ArrayList<Users> arrayList, String word){
        users = new ArrayList<>();
        getDataManager().getListUser()
                .orderByChild(AppConstants.USER_NAME_RES)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Users user = snapshot.getValue(Users.class);
                    for(int i = 0; i < arrayList.size(); i++){
                        if (user != null && user.getId().equals(arrayList.get(i).getId())) {
                            users.add(user);
                        }
                    }
                }
                sortListUser(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void sortListUser(ArrayList<Users> userArrayList) {
        userArrayList.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUser();
    }

    private void createChatRoom(Users receiver) {
        String roomKey = getDataManager().generateRoomKey();
        Room newRoom = new Room(roomKey, receiver.getUsername(),
                receiver.getImageURL(), receiver.getId(), 0,  "", getDataManager().getTimeStamp());
        getDataManager().createChatRoom(receiver, newRoom)
                .addOnSuccessListener(unused -> {
                    getMvpView().openChatRoom(newRoom);
                })
                .addOnFailureListener(e -> {
                    getMvpView().showMessage(e.getMessage());
                });
    }
}
