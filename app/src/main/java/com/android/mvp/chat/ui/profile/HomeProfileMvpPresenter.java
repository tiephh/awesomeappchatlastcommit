package com.android.mvp.chat.ui.profile;

import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;

public interface HomeProfileMvpPresenter <V extends HomeProfileMvpView & MvpView> extends MvpPresenter<V> {

    void showProfile();

    void signOut();
}
