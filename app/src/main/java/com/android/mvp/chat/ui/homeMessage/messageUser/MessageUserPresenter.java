package com.android.mvp.chat.ui.homeMessage.messageUser;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Message;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.fcm.Client;
import com.android.mvp.chat.fcm.Data;
import com.android.mvp.chat.fcm.MyResponse;
import com.android.mvp.chat.fcm.Sender;
import com.android.mvp.chat.fcm.Token;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageUserPresenter<V extends MessageUserMvpView> extends BasePresenter<V>
        implements MessageUserMvpPresenter<V> {

    private static final String TAG = MessageUserPresenter.class.getSimpleName();

    APIService apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

    private Users senderByMe;
    private Room room;

    @Inject
    public MessageUserPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void setRoom(Room room) {
        this.room = room;
        getDataSender();
        getDataChatRoom();
    }


    private void getDataSender() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        senderByMe = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
        getDataManager().getCurrentDataUser().keepSynced(true);
    }

    private void getDataChatRoom() {
        getDataManager().getChatRoomMessage().keepSynced(true);
        getDataManager().getChatRoomMessage().child(room.getId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        ArrayList<Message> messages = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Message message = snapshot.getValue(Message.class);
                            if (message != null) {
                                messages.add(message);
                            }
                        }
                        Log.d(TAG, "messages = " + messages.toString());
                        getMvpView().setDataMessage(messages, senderByMe.getId());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
        // Reset unread message
        getDataManager().updateUnreadMessage(room.getId())
                .addOnSuccessListener(unused -> room.setUnreadMessage(0));
    }

    @Override
    public void sendMessage(String message) {
        Message newMessage = new Message(senderByMe.getId(), message, getDataManager().getCurrentTime());
        getDataManager().addNewMessage(room.getId(), newMessage)
                .addOnSuccessListener(unused -> {
                    updateChatRoomImg(message, message);
                })
                .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
        sendNotification(message);

    }

    private void sendNotification(String message) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference(AppConstants.TOKENS);
        Query query = tokens.orderByKey().equalTo(room.getReceiver());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    Token token = dataSnapshot.getValue(Token.class);
                    Data data = new Data(getDataManager().getFirebaseUserId(), R.mipmap.ic_launcher, senderByMe.getUsername() + " : " + message, "New Message", room.getReceiver());
                    assert token != null;
                    Sender sender = new Sender(data, token.getToken());
                    apiService.sendNotifications(sender)
                            .enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if (response.code() == 200) {
                                        assert response.body() != null;
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {

                                }
                            });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    @Override
    public void sendImg(ArrayList<String> listImg) {
        StorageReference reference = FirebaseStorage.getInstance().getReference();
        for (int i = 0; i < listImg.size(); i++){
            Uri file = Uri.fromFile(new File(listImg.get(i)));
            StorageReference riversRef = reference.child("images/" + file.getLastPathSegment());
            riversRef.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            Message newMessage = new Message(senderByMe.getId(), uri.toString(), getDataManager().getCurrentTime());
                            getDataManager().addNewMessage(room.getId(), newMessage)
                                    .addOnSuccessListener(unused -> {
                                        String nameReceiver = senderByMe.getUsername() + " "+getMvpView().getContext().getString(R.string.send_img);
                                        updateChatRoomImg(getMvpView().getContext().getString(R.string.your_send), nameReceiver);
                                    })
                                    .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
                        }
                    });
                }
            });
        }
        sendNotification(getMvpView().getContext().getString(R.string.send_img));

    }

    @Override
    public void sendStick(String name) {
        Message newMessage = new Message(senderByMe.getId(), name, getDataManager().getCurrentTime());
        getDataManager().addNewMessage(room.getId(), newMessage)
                .addOnSuccessListener(unused -> {
                    String nameReceiver = senderByMe.getUsername() +" "+ getMvpView().getContext().getString(R.string.send_img);
                    updateChatRoomImg(getMvpView().getContext().getString(R.string.your_send), nameReceiver);
                })
                .addOnFailureListener(e -> getMvpView().showMessage(e.getMessage()));
        sendNotification( getMvpView().getContext().getString(R.string.send_img));
    }

    private void updateChatRoomImg(String sender, String receiver) {
        room.setLastMessage(sender);
        room.setTimeStamp(getDataManager().getTimeStamp());
        getDataManager().updateChatRoomSender(room);
        getDataManager().updateChatRoomReceiver(room)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Room room = dataSnapshot.getValue(Room.class);
                        if (room != null) {
                            room.setLastMessage(receiver);
                            int unread = room.getUnreadMessage() + 1;
                            room.setUnreadMessage(unread);
                            room.setThumbnail(senderByMe.getImageURL());
                            room.setName(senderByMe.getUsername());
                            room.setTimeStamp(getDataManager().getTimeStamp());
                            dataSnapshot.getRef().setValue(room);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }


}
