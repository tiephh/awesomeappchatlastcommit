package com.android.mvp.chat.ui.friend.tabFriend;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.HomeFriendFragment;
import com.android.mvp.chat.ui.homeMessage.messageUser.MessageUserFragment;
import com.android.mvp.chat.utils.AppConstants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabFriendFragment extends BaseFragment implements TabFriendMvpView {

    public static final String TAG = TabFriendFragment.class.getSimpleName();

    @BindView(R.id.recycler_view_tab_friend)
    RecyclerView recyclerViewTabFriend;

    TabFriendAdapter adapter;


    @Inject
    TabFriendMvpPresenter<TabFriendMvpView> mPresenter;

    public static TabFriendFragment newInstance(Bundle args) {
        TabFriendFragment fragment = new TabFriendFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_all_friend, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        mPresenter.readUser();
        HomeFriendFragment homeFriendFragment2 = (HomeFriendFragment) getParentFragment();

        if (homeFriendFragment2 != null){
            homeFriendFragment2.setOnTextChanged(new HomeFriendFragment.OnTextChanged() {
                @Override
                public void onTextChanged(String word) {
                    if (word != null) {
                        mPresenter.searchUser(word);
                        Log.d("aaaaa", word);//
                    }

                }
            });
        }
    }
    private void initAdapter(){
        recyclerViewTabFriend.setHasFixedSize(true);
        recyclerViewTabFriend.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TabFriendAdapter(getContext(), new ArrayList<>());
        recyclerViewTabFriend.setAdapter(adapter);
    }

    @Override
    public void onSuccess(ArrayList<SortUser> userArrayList) {
        adapter.setData(userArrayList);
        adapter.setOnClck(new TabFriendAdapter.IOnClick() {
            @Override
            public void onItemCLick(int i) {
                mPresenter.checkExistsRoom(userArrayList.get(i).getUsers());
            }
        });

    }

    @Override
    public void openChatRoom(Room room) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BundleKey.ROOM, room);
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.slide_left, R.anim.slide_right)
                    .add(R.id.frameContainer, MessageUserFragment.newInstance(bundle), MessageUserFragment.TAG)
                    .addToBackStack(MessageUserFragment.TAG)
                    .commit();
        }
    }
}
