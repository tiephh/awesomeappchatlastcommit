package com.android.mvp.chat.ui.profile;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HomeProfilePresenter <V extends HomeProfileMvpView> extends BasePresenter<V>
        implements HomeProfileMvpPresenter<V> {


    @Inject
    public HomeProfilePresenter(DataManager dataManager,
                                SchedulerProvider schedulerProvider,
                                CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void showProfile() {
        String email = getDataManager().getFirebaseUser().getEmail();
        getDataManager().getDatabaseReference(AppConstants.USERS)
                .child(getDataManager().getFirebaseUserId())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Users users = dataSnapshot.getValue(Users.class);
                        getMvpView().onSuccess(users, email);

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
        getDataManager().getDatabaseReference(AppConstants.USERS).child(getDataManager().getFirebaseUserId()).keepSynced(true);
    }

    @Override
    public void signOut() {
        getDataManager().getFirebaseAuth().signOut();
    }

}
