package com.android.mvp.chat.ui.register;

import com.android.mvp.chat.ui.base.MvpView;

public interface RegisterMvpView extends MvpView {
    void onSuccess();
    void onFail();
    void failPassWord();
    void failInputData();
    void failPolicy();
    void openHomeFragment();
}
