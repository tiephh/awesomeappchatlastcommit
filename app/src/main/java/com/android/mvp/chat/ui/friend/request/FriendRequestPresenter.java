package com.android.mvp.chat.ui.friend.request;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.ui.friend.tabFriend.TabFriendMvpPresenter;
import com.android.mvp.chat.ui.friend.tabFriend.TabFriendMvpView;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class FriendRequestPresenter <V extends FriendRequestMvpView> extends BasePresenter<V>
        implements FriendRequestMvpPresenter<V> {

    @Inject
    public FriendRequestPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
    Users sender;
    @Override
    public void readSender() {
        getDataSender();
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REQUEST_SEND)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readSender(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getDataSender() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sender = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }
    @Override
    public void readReceiver() {
        ArrayList<Users> userArrayList = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.FRIEND_REQUEST)
                .child(getDataManager().getFirebaseUserId())
                .child(AppConstants.REQUEST_RECEIVE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        userArrayList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && getDataManager().getFirebaseUser()!=null) {
                                if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                    userArrayList.add(user);
                                }
                            }
                        }
                        getMvpView().readReceiver(userArrayList);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
    private void removeValueSender(Users users, String id, String table){
        Query query = getDataManager().getDatabaseReference()
                .child(AppConstants.FRIEND_REQUEST)
                .child(id)
                .child(table)
                .orderByChild(AppConstants.ID).equalTo(users.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                    readSender();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void cancleRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REQUEST_SEND);
        removeValueSender(sender, users.getId(), AppConstants.REQUEST_RECEIVE);
    }

    @Override
    public void acceptRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REQUEST_RECEIVE);
        removeValueSender(sender, users.getId(), AppConstants.REQUEST_SEND);

        getDataManager().getDatabaseReference(AppConstants.FRIENDS)
                .child(getDataManager().getFirebaseUserId())
                .push()
                .setValue(users);
        getDataManager().getDatabaseReference(AppConstants.FRIENDS)
                .child(users.getId())
                .push()
                .setValue(sender);
    }

    @Override
    public void disagreeRequest(Users users) {
        removeValueSender(users, getDataManager().getFirebaseUserId(), AppConstants.REQUEST_RECEIVE);
        removeValueSender(sender, users.getId(), AppConstants.REQUEST_SEND);
    }
}
