package com.android.mvp.chat.ui.homeMessage;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.EventBusMes;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.HomeFriendFragment;
import com.android.mvp.chat.ui.homeMessage.messageUser.MessageUserFragment;
import com.android.mvp.chat.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeMessageFragment extends BaseFragment implements HomeMessageMvpView {

    public static final String TAG = HomeMessageFragment.class.getSimpleName();

    public static HomeMessageFragment newInstance() {
        Bundle args = new Bundle();
        HomeMessageFragment fragment = new HomeMessageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    HomeMessageMvpPresenter<HomeMessageMvpView> mPresenter;


    @BindView(R.id.rv_mes_home_mes)
    RecyclerView rvMesHome;

    @BindView(R.id.btn_create_mess)
    ImageView btnCreateMess;

    @BindView(R.id.et_search_messs)
    EditText etSearchMess;
    private RoomAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_mess_fragmet, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        hideKeyboard();
        btnCreateMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEventBus(1);
            }
        });
        etSearchMess.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mPresenter.searchRoom(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvMesHome.setLayoutManager(linearLayoutManager);
        adapter = new RoomAdapter(getContext(), new ArrayList<>());
        adapter.setOnItemClickListener(position -> {
            openChatRoom(adapter.getData().get(position));
        });
        rvMesHome.setAdapter(adapter);
    }

    private void openChatRoom(Room room) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.BundleKey.ROOM, room);
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right, R.anim.slide_left, R.anim.slide_right)
                    .add(R.id.frameContainer, MessageUserFragment.newInstance(bundle), MessageUserFragment.TAG)
                    .addToBackStack(MessageUserFragment.TAG)
                    .commit();
        }
    }

    private void sendEventBus(int mess){
        EventBus.getDefault().post(new EventBusMes(mess));
    }
    @Override
    public void addDataRooms(ArrayList<Room> rooms) {

        if (adapter != null) {
            Collections.sort(rooms, new Comparator<Room>() {
                @Override
                public int compare(Room room, Room t1) {
                    if (Long.parseLong(room.getTimeStamp()) < Long.parseLong(t1.getTimeStamp())) {
                        return 1;
                    } else {
                        if (Long.parseLong(room.getTimeStamp()) == Long.parseLong(t1.getTimeStamp())) {
                            return 0;
                        } else {
                            return -1;
                        }
                    }
                }
            });

            adapter.setData(rooms);
        }
    }
}
