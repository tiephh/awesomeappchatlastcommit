package com.android.mvp.chat.ui.homeMessage;

import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;

public interface HomeMessageMvpPresenter<V extends HomeMessageMvpView & MvpView> extends MvpPresenter<V> {
    void searchRoom(String word);
}
