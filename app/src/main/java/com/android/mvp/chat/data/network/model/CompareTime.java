package com.android.mvp.chat.data.network.model;

import java.util.Date;

public class CompareTime implements Comparable<CompareTime> {

    private Date dateTime;

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date datetime) {
        this.dateTime = datetime;
    }
    @Override
    public int compareTo(CompareTime compareTime) {
        return getDateTime().compareTo(compareTime.getDateTime());
    }
}
