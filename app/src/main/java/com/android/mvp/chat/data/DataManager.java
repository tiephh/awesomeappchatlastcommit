/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.mvp.chat.data;


import com.android.mvp.chat.data.db.DbHelper;
import com.android.mvp.chat.data.firebase.Message;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.data.network.ApiHelper;
import com.android.mvp.chat.data.prefs.PreferencesHelper;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by janisharali on 27/01/17.
 */

public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {

    void updateApiHeader(Long userId, String accessToken);

    void setUserAsLoggedOut();

    DatabaseReference getDatabaseReference();

    DatabaseReference getDatabaseReference(String table);

    FirebaseUser getFirebaseUser();

    String getFirebaseUserId();

    FirebaseAuth getFirebaseAuth();

    String getCurrentTime();

    Task<AuthResult> getSignInFirebase(String email, String pass);

    Task<AuthResult> getCreateFirebase(String email, String pass);

    void updateUserInfo(
            String accessToken,
            Long userId,
            LoggedInMode loggedInMode,
            String userName,
            String email,
            String profilePicPath);

    void setDataUser(Users user);

    Users getDataUser();

    DatabaseReference getListRoomByUser();
    FirebaseDatabase getListRoomOffline();

    Task<DataSnapshot> getListRoomByUserOnceTime();

    String generateRoomKey();

    Task<Void> createChatRoom(Users receiver, Room newRoom);

    DatabaseReference getChatRoomMessage();

    Task<Void> updateUnreadMessage(String roomId);

    DatabaseReference getCurrentDataUser();

    Task<Void> sendRequestFrSender(Users users);

    Task<Void> sendRequestFrReceiver(Users users, Users sender);

    Task<Void> removeValueSender(Users users);

    Task<Void> removeValueReceiver(Users users);

    Task<Void> addNewMessage(String receiverId, Message message);

    Task<Void> updateChatRoomSender(Room room);

    String getTimeStamp();

    DatabaseReference updateChatRoomReceiver(Room room);

    DatabaseReference getListFriendByUser();

    DatabaseReference getListUser();

    DatabaseReference getListFriendRequestSend();

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_SERVER(1);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }
}
