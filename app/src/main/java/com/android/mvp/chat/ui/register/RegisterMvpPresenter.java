package com.android.mvp.chat.ui.register;

import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;


public interface RegisterMvpPresenter <V extends RegisterMvpView & MvpView> extends MvpPresenter<V> {
    void onRegister(String userName, String email, String pass, boolean isCheck);
    void onLogin(String user, String pass);
}
