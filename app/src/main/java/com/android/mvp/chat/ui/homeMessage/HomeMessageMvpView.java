package com.android.mvp.chat.ui.homeMessage;

import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface HomeMessageMvpView extends MvpView {
    void addDataRooms(ArrayList<Room> rooms);
}
