package com.android.mvp.chat.ui.profile;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

public interface HomeProfileMvpView extends MvpView {
    void onSuccess(Users users, String email);
}
