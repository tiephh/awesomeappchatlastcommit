package com.android.mvp.chat.ui.friend.tabUser;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TabUserPresenter<V extends TabUserMvpView> extends BasePresenter<V>
        implements TabUserMvpPresenter<V> {

    @Inject
    public TabUserPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    Users sender;

    private List<Users> friends = new ArrayList<>();
    private List<Users> friendRequestSends = new ArrayList<>();
    private List<Users> users = new ArrayList<>();
    private ArrayList<UserSort> userSorts;
    private ArrayList<UserSort> userSortsSearch;
    private ArrayList<Users> userArrayList;

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        getListFriend();
        getDataSender();
    }

    private void getListFriend() {
        getDataManager().getListFriendByUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        friends = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null) {
                                friends.add(user);
                            }
                        }
                        getListFriendRequestSend();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().onError(databaseError.getMessage());
                    }
                });
    }

    private void getListFriendRequestSend() {
        getDataManager().getListFriendRequestSend()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        friendRequestSends = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null) {
                                friendRequestSends.add(user);
                            }
                        }
                        getListUser();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void getListUser() {
        getDataManager().getListUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        users = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Users user = snapshot.getValue(Users.class);
                            if (user != null && !user.getId().equals(getDataManager().getFirebaseUserId())) {
                                users.add(user);
                            }
                        }
                        sortListUserWithAlphabet();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().onError(databaseError.getMessage());
                    }
                });
    }

    private void sortListUserWithAlphabet() {
        users.sort(Comparator.comparing(user -> String.valueOf(user.getUsername().charAt(0)).toUpperCase()));
        filterListUser();
    }

    private void filterListUser() {
        userSorts = new ArrayList<>();
        String lastHeader = "";
        for (Users user : users) {
            String header = String.valueOf(user.getUsername().charAt(0)).toUpperCase();
            AppConstants.StateUser currentState = getStateUser(user);
            if (!header.equals(lastHeader)) {
                lastHeader = header;
                userSorts.add(new UserSort(header, null, true, currentState));
            }
            userSorts.add(new UserSort(header, user, false, currentState));
        }
        getMvpView().onSuccess(userSorts);
    }

    private AppConstants.StateUser getStateUser(Users user) {
        AppConstants.StateUser state = AppConstants.StateUser.OTHERS;
        for (Users friend : friends) {
            if (user.getId().equals(friend.getId())) {
                state = AppConstants.StateUser.FRIEND;
            }
        }
        for (Users friendRequest : friendRequestSends) {
            if (user.getId().equals(friendRequest.getId())) {
                state = AppConstants.StateUser.REQUEST_FRIEND;
            }
        }
        return state;
    }


    private void getDataSender() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        sender = dataSnapshot.getValue(Users.class);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

    private void removeValueSender(Users users, String id, String table) {
        Query query = getDataManager().getDatabaseReference()
                .child(AppConstants.FRIEND_REQUEST)
                .child(id)
                .child(table)
                .orderByChild(AppConstants.ID).equalTo(users.getId());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void sendRequestFr(Users user) {
        getDataManager().sendRequestFrSender(user);
        getDataManager().sendRequestFrReceiver(user, sender);
    }

    @Override
    public void cancleRequestFr(Users user) {
        removeValueSender(user, getDataManager().getFirebaseUserId(), AppConstants.REQUEST_SEND);
        removeValueSender(sender, user.getId(), AppConstants.REQUEST_RECEIVE);
    }

    @Override
    public void searchUser(String word) {
        users = new ArrayList<>();
        getDataManager().getDatabaseReference(AppConstants.USERS)
                .orderByChild(AppConstants.USER_NAME_RES)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        users.clear();
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            Users user = dataSnapshot.getValue(Users.class);
                            assert user != null;
                            if (!user.getId().equals(getDataManager().getFirebaseUserId())) {
                                users.add(user);
                            }
                        }
                        sortListUserWithAlphabet();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

}
