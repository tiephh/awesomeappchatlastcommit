package com.android.mvp.chat.ui.profile.changePofile;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

public interface ChangeProfileMvpView extends MvpView {
    void setUpView(Users users);
}
