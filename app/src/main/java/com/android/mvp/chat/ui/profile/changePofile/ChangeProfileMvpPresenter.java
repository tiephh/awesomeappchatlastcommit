package com.android.mvp.chat.ui.profile.changePofile;

import android.net.Uri;

import com.android.mvp.chat.ui.base.MvpPresenter;

public interface ChangeProfileMvpPresenter <V extends ChangeProfileMvpView> extends MvpPresenter<V> {


    void uploadImage(Uri mUri);

    void uploadName(String name);
}