package com.android.mvp.chat.ui.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.profile.changePofile.ChangeProfileFragment;
import com.android.mvp.chat.ui.splash.SplashFragment;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.LocaleHelper;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeProfileFragment extends BaseFragment implements HomeProfileMvpView {

    public static final String TAG = HomeProfileFragment.class.getSimpleName();

    @BindView(R.id.img_profile)
    ImageView imgProfile;

    @BindView(R.id.img_circle_profile)
    CircleImageView imgCircleProfile;

    @BindView(R.id.txt_name_profile)
    TextView txtNameProfile;

    @BindView(R.id.txt_email_profile)
    TextView txtEmailProfile;

    @BindView(R.id.img_change_profile)
    ImageView imgChangeProfile;

    @BindView(R.id.txt_sign_out)
    TextView txtSignOut;

    @BindView(R.id.img_sign_out)
    ImageView imgSignOut;

    @BindView(R.id.tv_laguage)
    TextView tvLanguage;

    @Inject
    HomeProfileMvpPresenter<HomeProfileMvpView> mPresenter;


    public static HomeProfileFragment newInstance() {
        HomeProfileFragment fragment = new HomeProfileFragment();
        return fragment;
    }

    public static HomeProfileFragment newInstance(Bundle args) {
        HomeProfileFragment fragment = new HomeProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mPresenter.showProfile();
        tvLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeLanguageDialog();
            }
        });
        imgChangeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                            .add(R.id.frameContainer, ChangeProfileFragment.newInstance(), ChangeProfileFragment.TAG)
                            .addToBackStack(ChangeProfileFragment.TAG)
                            .commit();
                }
            }
        });

        txtSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });

        imgSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();
            }
        });
    }

    private void showChangeLanguageDialog() {
        AppConstants.AppLanguage[] appLanguages = AppConstants.AppLanguage.values();
        String[] items = AppConstants.AppLanguage.getListLanguage();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setTitle(R.string.choose_language)
                .setSingleChoiceItems(items, getCheckedLanguage(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i != getCheckedLanguage()) {
                            setLocale(appLanguages[i].getLocale());
                        }
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private int getCheckedLanguage() {
        return AppConstants.AppLanguage.getIndexLanguage(LocaleHelper.getLanguage(getActivity()));
    }

    private void setLocale(String locale) {
        LocaleHelper.setLocale(getActivity().getBaseContext(), locale);
        Intent intent = getActivity().getIntent();
        getActivity().finish();
        startActivity(intent);
    }

    @Override
    public void onSuccess(Users users, String email) {
        txtNameProfile.setText(users.getUsername());
        txtEmailProfile.setText(email);
        if (users.getImageURL().equals(AppConstants.DEFAULT)) {
            Glide.with(getContext()).load(R.mipmap.ic_launcher).into(imgCircleProfile);
            Glide.with(getContext()).load(R.mipmap.ic_launcher).into(imgProfile);
        } else {
            Glide.with(getContext()).load(users.getImageURL()).into(imgCircleProfile);
            Glide.with(getContext()).load(users.getImageURL()).into(imgProfile);
        }
    }

    private void alertDialog() {
        new AlertDialog.Builder(getContext())
                .setTitle(getActivity().getString(R.string.signout))
                .setMessage(getActivity().getString(R.string.want_signout))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void signOut() {
        mPresenter.signOut();
        if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                    .replace(R.id.frameContainer, SplashFragment.newInstance(), SplashFragment.TAG)
                    .commit();
        }
    }
}