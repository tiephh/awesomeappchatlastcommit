package com.android.mvp.chat.ui.home;

import com.android.mvp.chat.ui.base.MvpPresenter;

public interface HomeMvpPresenter<V extends HomeMvpView> extends MvpPresenter<V> {
    void readRooms();
    void readRequest();
}
