package com.android.mvp.chat.ui.friend.request;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.homeMessage.messageUser.sendImg.PhotoAdapter;
import com.android.mvp.chat.utils.AppConstants;
import com.bumptech.glide.Glide;
import com.daimajia.swipe.SwipeLayout;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReceiverAdapter extends RecyclerView.Adapter<ReceiverAdapter.ReceiverViewHolder>{

    private Context mContext;
    private ArrayList<Users> data;

    public ReceiverAdapter(Context mContext, ArrayList<Users> data) {
        this.mContext = mContext;
        this.data = data;
    }
    public interface IOnClickItem{
        void itemClick(Users users);
        void deleteItem(Users users);
    }
    public IOnClickItem iOnClickItem;

    public void setOnClickItem(IOnClickItem iOnClickItem) {
        this.iOnClickItem = iOnClickItem;
    }

    public void setData(ArrayList<Users> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ReceiverViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.swipe_layout, viewGroup, false);
        return new ReceiverAdapter.ReceiverViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceiverViewHolder receiverViewHolder, int i) {
        Users users = data.get(i);
        receiverViewHolder.bindView(users, iOnClickItem);
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    class ReceiverViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivc_avatar)
        CircleImageView ivcAvater;

        @BindView(R.id.tv_user_name_request)
        TextView txtName;

        @BindView(R.id.btn_agree_friend_request)
        AppCompatButton btnAgreeFriend;

        @BindView(R.id.swipe_layout)
        SwipeLayout swipeLayout;

        public ReceiverViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        public void bindView(Users users, IOnClickItem iOnClickItem){
            txtName.setText(users.getUsername());
            if (users.getImageURL().equals(AppConstants.DEFAULT)) {
                Glide.with(mContext).load(R.drawable.grapefruit);
            } else {
                Glide.with(mContext).load(users.getImageURL()).circleCrop().into(ivcAvater);
            }
            btnAgreeFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClickItem != null){
                        iOnClickItem.itemClick(users);
                    }
                }
            });
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.layout_disagree));
            swipeLayout.findViewById(R.id.tv_disagree).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(iOnClickItem != null){
                        iOnClickItem.deleteItem(users);
                    }
                }
            });
        }
    }
}
