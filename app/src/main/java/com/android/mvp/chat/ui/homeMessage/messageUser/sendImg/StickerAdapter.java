package com.android.mvp.chat.ui.homeMessage.messageUser.sendImg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StickerAdapter extends RecyclerView.Adapter<StickerAdapter.StickerViewHolder>{

    private Context mContext;
    private ArrayList<String> mListPhoto;


    public StickerAdapter(Context mContext, ArrayList<String> mListPhoto) {
        this.mContext = mContext;
        this.mListPhoto = mListPhoto;
    }
    public interface IOnClick{
        void itemClick(String name);
    }
    public PhotoAdapter.IOnClick iOnClick;

    public void setOnClick(PhotoAdapter.IOnClick iOnClick){
        this.iOnClick = iOnClick;
    }
    @NonNull
    @Override
    public StickerAdapter.StickerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_sticker, viewGroup, false);
        return new StickerAdapter.StickerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StickerViewHolder stickerViewHolder, int i) {

        String image = mListPhoto.get(i);
        stickerViewHolder.bindView(image, iOnClick);

    }

    @Override
    public int getItemCount() {
        if(mListPhoto == null){
            return 0;
        }else {
            return mListPhoto.size();
        }
    }
    public class StickerViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.iv_sticker)
        ImageView ivSticker;
        public StickerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
        public void bindView(String image, PhotoAdapter.IOnClick iOnClick){
            Glide.with(mContext).load(image).into(ivSticker);
            ivSticker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(iOnClick != null){
                        iOnClick.itemClick(image);
                    }
                }
            });
        }
    }
}
