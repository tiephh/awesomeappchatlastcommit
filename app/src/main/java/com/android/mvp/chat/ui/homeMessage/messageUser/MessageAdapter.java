package com.android.mvp.chat.ui.homeMessage.messageUser;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Message;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.DateUtils;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Message> data;
    private String imageURL;
    private String uId;


    public static final int MSG_LEFT = 0;
    public static final int MSG_RIGHT = 1;
    public static final int MSG_LEFT_IMG = 2;
    public static final int MSG_RIGHT_IMG = 3;

    public MessageAdapter(Context context, ArrayList<Message> data) {
        this.context = context;
        this.data = data;
    }

    public void setImg(String imageURL){
        this.imageURL = imageURL;
    }

    public void setData(ArrayList<Message> data, String uId) {
        this.data = data;
        this.uId = uId;
        notifyDataSetChanged();
    }

    public String getTime(int pos) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date dateTime = sdf.parse(data.get(pos).getTime());

        DateUtils currentTime = new  DateUtils(Calendar.getInstance().getTime());
        DateUtils messTime = new  DateUtils(dateTime);

        String result = currentTime.getYear();
        String time = messTime.getYear();

        String month = messTime.getMonth();
        String month2 = currentTime.getMonth();

        String date = messTime.getDate();
        String date2 = currentTime.getDate();
        if (month.equals(month2)) {
            if (Integer.parseInt(date) - Integer.parseInt(date2) == 1) {
                return context.getString(R.string.yesterday);
            } else if (Integer.parseInt(date) - Integer.parseInt(date2) > 1) {
                return messTime.getYear();
            }else if(Integer.parseInt(date) - Integer.parseInt(date2) == 0){
                return context.getString(R.string.today);
            } else {
                return context.getString(R.string.today);
            }
        }else if(result.equals(time)){
            return context.getString(R.string.today);
        } else {
            return messTime.getYear();
        }
    }

    //By using function below getViewType() we can check and change layout according to sender and receiver
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == MSG_RIGHT) {
            view = LayoutInflater.from(context).inflate(R.layout.chat_item_right, parent, false);
        }else if(viewType == MSG_LEFT){
            view = LayoutInflater.from(context).inflate(R.layout.chat_item_left, parent, false);
        }else if(viewType == MSG_LEFT_IMG){
            view = LayoutInflater.from(context).inflate(R.layout.chat_item_img_left, parent, false);
        }else {
            view = LayoutInflater.from(context).inflate(R.layout.chat_item_img_right, parent, false);
        }

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Message message = data.get(position);
        try {
            Date date1 =new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(message.getTime());
            SimpleDateFormat hour = new SimpleDateFormat("HH:mm");
            String time = hour.format(date1);
            if(message.getContent().startsWith("https://")){
                Glide.with(context).load(message.getContent()).into(holder.chatImg);
                if (position == data.size() - 1) {
                    holder.text_seen.setVisibility(View.VISIBLE);
                    holder.text_seen.setText(time);
                } else {
                    holder.text_seen.setVisibility(View.GONE);
                }
            }else{
                holder.message.setText(message.getContent());
                if (position == data.size() - 1) {
                    holder.text_seen.setVisibility(View.VISIBLE);
                    holder.text_seen.setText(time);
                } else {
                    holder.text_seen.setVisibility(View.GONE);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (imageURL != null) {
            if (imageURL.equals(AppConstants.DEFAULT))
                Glide.with(context).load(R.mipmap.ic_launcher);
            else
                Glide.with(context).load(imageURL).into(holder.senderProfilePicture);
        }

    }


    @Override
    public int getItemCount() {
        return this.data.size();
    }

    // This class used as view holder in recycle view list to preview users
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // these are widget located in left and right chat layout
        // we see that they are both with the same name so that we can change between left and right easily

        AppCompatTextView message;
        TextView text_seen;
        ImageView senderProfilePicture, chatImg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            message = itemView.findViewById(R.id.chat_message);
            senderProfilePicture = itemView.findViewById(R.id.image_chatting);
            text_seen = itemView.findViewById(R.id.seen);
            chatImg = itemView.findViewById(R.id.chat_img);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (data.get(position).getSenderId().equals(uId)) {
            if (data.get(position).getContent().startsWith("https://")){
                return MSG_RIGHT_IMG;
            }else {
                return MSG_RIGHT;
            }

        } else
            if(data.get(position).getContent().startsWith("https://")) {
                return MSG_LEFT_IMG;
            }else {
                return MSG_LEFT;
            }

    }
}
