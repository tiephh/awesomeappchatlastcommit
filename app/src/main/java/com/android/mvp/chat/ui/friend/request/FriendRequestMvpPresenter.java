package com.android.mvp.chat.ui.friend.request;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;

public interface FriendRequestMvpPresenter <V extends FriendRequestMvpView & MvpView> extends MvpPresenter<V> {
    void readSender();
    void readReceiver();
    void cancleRequest(Users users);
    void acceptRequest(Users users);
    void disagreeRequest(Users users);
}
