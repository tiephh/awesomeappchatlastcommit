package com.android.mvp.chat.ui.friend.tabFriend;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.friend.tabUser.TabUserAdapter;
import com.android.mvp.chat.ui.friend.tabUser.UserSort;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.listener.OnItemClickListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TabFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<SortUser> userList;

    public static final int SECTION_VIEW = 1;
    public static final int CONTENT_VIEW = 2;

    public interface IOnClick {
        void onItemCLick(int i);
    }
    public void setData(ArrayList<SortUser> userList){
        this.userList = userList;
        notifyDataSetChanged();
    }

    public IOnClick iOnClick;

    void setOnClck(IOnClick iOnClick) {
        this.iOnClick = iOnClick;
    }

    public TabFriendAdapter(Context context, List<SortUser> userList) {
        this.context = context;
        this.userList = userList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view;

        switch (viewType) {
            case SECTION_VIEW:
                view = LayoutInflater.from(context).inflate(R.layout.item_user_header, viewGroup, false);
                return new MyViewHolderHead(view);
            default:
                view = LayoutInflater.from(context).inflate(R.layout.item_tab_friend, viewGroup, false);
                return new MyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        SortUser user = userList.get(i);
        switch (viewHolder.getItemViewType()) {
            case SECTION_VIEW:
                MyViewHolderHead myViewHolderHead = (MyViewHolderHead) viewHolder;
                myViewHolderHead.bindView(user);
                break;
            case CONTENT_VIEW:
                MyViewHolder myViewHolder = (MyViewHolder) viewHolder;
                myViewHolder.bindView(user, iOnClick);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if (userList.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.col_user_friend)
        TextView txtUsername;
        @BindView(R.id.ivc_avatar)
        CircleImageView imgCircleFriend;

        @BindView(R.id.layout_tab_fr)
        LinearLayout layoutTabFr;



        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindView(SortUser user, IOnClick iOnClick) {

            txtUsername.setText(user.getUsers().getUsername());
            layoutTabFr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (iOnClick != null) {
                        iOnClick.onItemCLick(getAdapterPosition());
                    }
                }
            });

            if (user.getUsers().getImageURL().equals(AppConstants.DEFAULT)) {
                Glide.with(context).load(R.mipmap.ic_launcher).circleCrop().into(imgCircleFriend);
            } else {
                Glide.with(context).load(user.getUsers().getImageURL()).circleCrop().into(imgCircleFriend);
            }

        }
    }

    public class MyViewHolderHead extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_header)
        TextView txtHeader;


        public MyViewHolderHead(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindView(SortUser user) {
            txtHeader.setText(user.getHeader());
        }
    }
}

