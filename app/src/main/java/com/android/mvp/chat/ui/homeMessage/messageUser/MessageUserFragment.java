package com.android.mvp.chat.ui.homeMessage.messageUser;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Message;
import com.android.mvp.chat.data.firebase.Room;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.homeMessage.messageUser.sendImg.PhotoAdapter;
import com.android.mvp.chat.ui.homeMessage.messageUser.sendImg.StickerAdapter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.ImageGallery;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessageUserFragment extends BaseFragment implements MessageUserMvpView {


    public static final String TAG = MessageUserFragment.class.getSimpleName();


    @Inject
    MessageUserPresenter<MessageUserMvpView> mPresenter;


    MessageAdapter messageAdapter;
    PhotoAdapter photoAdapter;

    @BindView(R.id.chat_recycleview)
    RecyclerView chatRecycleView;

    @BindView(R.id.img_circle_messenger)
    CircleImageView ivThumbnail;

    @BindView(R.id.txt_username_messenger)
    TextView tvRoomName;

    @BindView(R.id.img_back_messenger)
    ImageButton imgBackMessenger;

    @BindView(R.id.img_send)
    ImageView imgSend;

    @BindView(R.id.edt_send)
    EditText edtSend;

    @BindView(R.id.gv_galley)
    RecyclerView ivGallery;

    @BindView(R.id.btn_gallery)
    ImageView btnGallery;

    @BindView(R.id.ct_layout)
    ConstraintLayout ctLayout;

    @BindView(R.id.btn_cancle)
    Button btnCancle;

    @BindView(R.id.btn_send_img)
    Button btnSendImg;

    @BindView(R.id.ct_layout_sticker)
    ConstraintLayout ctLayoutSticker;

    @BindView(R.id.btn_sticker)
    ImageButton btnSticker;

    @BindView(R.id.gv_sticker)
    RecyclerView gvSticker;

    @BindView(R.id.tv_time_chat)
    TextView tvTimeChat;


    public static MessageUserFragment newInstance() {
        Bundle args = new Bundle();
        MessageUserFragment fragment = new MessageUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static MessageUserFragment newInstance(Bundle args) {
        MessageUserFragment fragment = new MessageUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null){
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        initData();

        edtSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctLayoutSticker.setVisibility(View.GONE);
                ctLayout.setVisibility(View.GONE);
            }
        });
        imgBackMessenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
                hideKeyboard();
            }
        });
        imgSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String msg = edtSend.getText().toString();
                    if (!msg.equals("")) {
                        mPresenter.sendMessage(msg);
                    } else {
                        Toast.makeText(getActivity(), R.string.dont_input, Toast.LENGTH_SHORT).show();
                    }
                    edtSend.setText("");

            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //mPresenter.requestImg();
                checkPermision();
                ctLayoutSticker.setVisibility(View.GONE);
                hideKeyboard();
                if (ctLayout.getVisibility() == View.VISIBLE){
                    ctLayout.setVisibility(View.GONE);
                    btnGallery.setImageResource(R.drawable.ic_photo_1);
                }else {
                    ctLayout.setVisibility(View.VISIBLE);
                    btnGallery.setImageResource(R.drawable.ic_add_photo);
                }
            }
        });
        btnSendImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(photoAdapter.getListImg() != null){
                    mPresenter.sendImg(photoAdapter.getListImg());
                }
                photoAdapter.clearList();
                btnCancle.setVisibility(View.GONE);
                btnSendImg.setVisibility(View.GONE);

            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctLayout.setVisibility(View.GONE);
                btnCancle.setVisibility(View.GONE);
                btnSendImg.setVisibility(View.GONE);
                photoAdapter.clearList();
            }
        });
        btnSticker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ctLayoutSticker.getVisibility() == View.VISIBLE){
                    ctLayoutSticker.setVisibility(View.GONE);
                    btnSticker.setImageResource(R.drawable.ic_smile_1);
                }else {
                    ctLayoutSticker.setVisibility(View.VISIBLE);
                    btnSticker.setImageResource(R.drawable.ic_smile);
                }
                ctLayout.setVisibility(View.GONE);
                loadSticker();
                hideKeyboard();
            }
        });
    }

    private void checkPermision(){
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppConstants.MY_READ_PERMISSION_CODE);
        }else {
            loadImages();
        }
    }
    private void loadSticker(){
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        gvSticker.setLayoutManager(gridLayoutManager);
        gvSticker.setFocusable(false);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(AppConstants.stickers);
        StickerAdapter stickerAdapter = new StickerAdapter(getContext(), arrayList);
        stickerAdapter.setOnClick(new PhotoAdapter.IOnClick() {
            @Override
            public void itemClick(String name) {
                ctLayoutSticker.setVisibility(View.GONE );
                mPresenter.sendStick(name);
            }
        });
        gvSticker.setAdapter(stickerAdapter);
    }

    private void loadImages() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        ivGallery.setLayoutManager(gridLayoutManager);
        ivGallery.setFocusable(false);
        List<String> List = ImageGallery.lisOfImages(getActivity());
        photoAdapter = new PhotoAdapter(getContext(), List);
        photoAdapter.setOnClick(new PhotoAdapter.IOnClick() {
            @Override
            public void itemClick(String name) {
                if(photoAdapter.getListImg().size() > 0 ){
                    btnCancle.setVisibility(View.VISIBLE);
                    btnSendImg.setVisibility(View.VISIBLE);
                }else {
                    btnCancle.setVisibility(View.GONE);
                    btnSendImg.setVisibility(View.GONE);
                }
            }
        });
        ivGallery.setAdapter(photoAdapter);
    }

    private void initData() {
        if (getArguments() != null && getArguments().containsKey(AppConstants.BundleKey.ROOM)) {
            Room room = (Room) getArguments().getSerializable(AppConstants.BundleKey.ROOM);
            if (room != null) {
                mPresenter.setRoom(room);
                tvRoomName.setText(room.getName());
                messageAdapter.setImg(room.getThumbnail());
                if (getContext() != null) {
                    Glide.with(getContext())
                            .load(room.getThumbnail().equals(AppConstants.DEFAULT)
                                    ? R.drawable.grapefruit : room.getThumbnail())
                            .into(ivThumbnail);
                }
            }
        }
    }

    private void initAdapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        chatRecycleView.setLayoutManager(linearLayoutManager);
        messageAdapter = new MessageAdapter(getContext(), new ArrayList<>());
        chatRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    RecyclerView.LayoutManager lm = chatRecycleView.getLayoutManager();
                    if (lm != null && lm instanceof LinearLayoutManager) {
                        int currPosition = ((LinearLayoutManager) lm).findFirstVisibleItemPosition();
                        String time = null;
                        try {
                            time = messageAdapter.getTime(currPosition);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        tvTimeChat.setVisibility(View.VISIBLE);
                        tvTimeChat.setText(time);
                    }
                }
            }
        });

        chatRecycleView.setAdapter(messageAdapter);


    }


    @Override
    public void setDataMessage(ArrayList<Message> arrayList, String uId) {
        if (messageAdapter != null) {
            messageAdapter.setData(arrayList, uId);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == AppConstants.MY_READ_PERMISSION_CODE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();
                loadImages();
            }else {
                Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
