package com.android.mvp.chat.fcm;

public class Sender {
    Data data;
    String to;

    public Sender(Data data, String to) {
        this.data = data;
        this.to = to;
    }

    public Sender() {
    }
}
