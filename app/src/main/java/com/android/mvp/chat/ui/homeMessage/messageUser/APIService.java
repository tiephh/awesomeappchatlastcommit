package com.android.mvp.chat.ui.homeMessage.messageUser;

import com.android.mvp.chat.fcm.MyResponse;
import com.android.mvp.chat.fcm.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAMxTI5uk:APA91bEUfcRb_0ze397WrcPza45bcbxolXH_gkjli0UEFxkw_GgW4NF-GLQWCCDjaSIsm03Q7r5VSd3AAnF8DRYpD8JmFnWwlgwJFtqAHVbUh434vO8s7AoDg4LZFlCS7N2fFQRP7g2V"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotifications(@Body Sender body);
}
