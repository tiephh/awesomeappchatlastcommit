package com.android.mvp.chat.ui.friend.tabFriend;

import com.android.mvp.chat.data.firebase.Users;

public class SortUser {
    private String header;
    private Users users;
    private  boolean isSection;

    public SortUser(String header, Users users, boolean isSection) {
        this.header = header;
        this.users = users;
        this.isSection = isSection;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        header = header;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }
}

