package com.android.mvp.chat.ui.friend;

import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;


public interface HomFriendMvpPresenter <V extends HomeFriendMvpView & MvpView> extends MvpPresenter<V> {
    void readRequest();
}
