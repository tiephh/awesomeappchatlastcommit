package com.android.mvp.chat.ui.friend.request;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.di.component.ActivityComponent;
import com.android.mvp.chat.ui.base.BaseFragment;
import com.android.mvp.chat.ui.friend.tabFriend.TabFriendFragment;
import com.android.mvp.chat.ui.homeMessage.RoomAdapter;


import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendRequestFragment extends BaseFragment implements FriendRequestMvpView {

    public static final String TAG = FriendRequestFragment.class.getSimpleName();

    @Inject
    FriendRequestPresenter<FriendRequestMvpView> mPresenter;

    private ReceiverAdapter adapterReceiver;
    private SenderAdapter adapterSender;

    @BindView(R.id.rv_request_friend)
    RecyclerView rvRequestFriend;

    @BindView(R.id.rv_send_request_friend)
    RecyclerView rvSendRequestFriend;

    public static FriendRequestFragment newInstance(Bundle args) {
        FriendRequestFragment fragment = new FriendRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friend_request_fragment, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setUnBinder(ButterKnife.bind(this, view));
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        initAdapter();
        mPresenter.readSender();
        mPresenter.readReceiver();
    }
    private void initAdapter(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rvRequestFriend.setLayoutManager(linearLayoutManager);
        adapterReceiver = new ReceiverAdapter(getContext(), new ArrayList<>());
        rvRequestFriend.setAdapter(adapterReceiver);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext());
        rvSendRequestFriend.setLayoutManager(linearLayoutManager1);
        adapterSender = new SenderAdapter(getContext(), new ArrayList<>());
        rvSendRequestFriend.setAdapter(adapterSender);

        adapterReceiver.setOnClickItem(new ReceiverAdapter.IOnClickItem() {
            @Override
            public void itemClick(Users users) {
                mPresenter.acceptRequest(users);
            }

            @Override
            public void deleteItem(Users users) {
                mPresenter.disagreeRequest(users);
            }
        });

        adapterSender.setOnClickItemSender(new SenderAdapter.IOnClickItemSender() {
            @Override
            public void itemClick(Users users) {
                mPresenter.cancleRequest(users);
            }
        });
    }

    @Override
    public void readSender(ArrayList<Users> users) {
        adapterSender.setData(users);
    }

    @Override
    public void readReceiver(ArrayList<Users> users) {

        adapterReceiver.setData(users);
    }
}
