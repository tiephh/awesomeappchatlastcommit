package com.android.mvp.chat.ui.friend.tabUser;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpPresenter;
import com.android.mvp.chat.ui.base.MvpView;

public interface TabUserMvpPresenter<V extends TabUserMvpView & MvpView> extends MvpPresenter<V> {
    void sendRequestFr(Users User);
    void cancleRequestFr(Users User);
    void searchUser(String word);
}
