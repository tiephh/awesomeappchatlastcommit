package com.android.mvp.chat.ui.friend.tabFriend;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpPresenter;

public interface TabFriendMvpPresenter<V extends TabFriendMvpView> extends MvpPresenter<V> {
    void readUser();
    void checkExistsRoom(Users receiver);

    void searchUser(String word);
}
