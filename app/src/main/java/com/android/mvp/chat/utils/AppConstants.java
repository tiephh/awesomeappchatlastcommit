/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.mvp.chat.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by amitshekhar on 08/01/17.
 */

public final class AppConstants {
    public static final String USER_ID = "userid";
    public static final String IMG_URL = "imgURL";
    public static final String USER_NAME = "userName";
    public static final String DEFAULT = "default";

    public static final String LASTMES = "lastMes";
    public static final String NAME = "name";
    public static final String UNREAD = "unread";

    public static final String USER_NAME_RES = "username";
    public static final String IMG_RES = "imageURL";
    public static final String ID_RES = "id";
    public static final String USERS = "Users";
    public static final String USER_RECEIVER = "userReceiver";
    public static final String FRIENDS = "Friends";
    public static final String CHATS = "Chats";

    public static final String SENDER = "sender";
    public static final String RECEIVER = "receiver";
    public static final String MESSAGE = "message";
    public static final String CHATLIST = "ChatList";
    public static final String SEEN = "seen";

    public static final String TIME_CHAT = "time";
    public static final String ID = "id";
    public static final String TOKENS = "Tokens";
    public static final String CHAT_NULL = "Chat object is null";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "mindorks_mvp.db";
    public static final String PREF_NAME = "mindorks_pref";

    public static final long NULL_INDEX = -1L;

    public static final String SEED_DATABASE_OPTIONS = "seed/options.json";
    public static final String SEED_DATABASE_QUESTIONS = "seed/questions.json";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    public static final int MY_READ_PERMISSION_CODE = 101;

    public static final String FRIEND_REQUEST = "friend_request";

    public static final String REQUEST_RECEIVE = "request_receive";
    public static final String REQUEST_SEND = "request_send";

    public static class ReferencePath {
        public static final String USERS = "Users";
        public static final String ROOMS = "Rooms";
        public static final String MEMBERS = "Members";
        public static final String MESSAGES = "Messages";
        public static final String FRIENDS = "Friends";
        public static final String FRIEND_REQUEST = "friend_request";
        public static final String REQUEST_SEND = "request_send";
        public static final String REQUEST_RECEIVE = "request_receive";
    }

    public static class BundleKey {
        public static final String ROOM = "ROOM";
    }

    public static List stickers = Arrays.asList("https://fcbk.su/_data/stickers/daily_duncan_vol1/daily_duncan_vol1_02.png",
            "https://1.bp.blogspot.com/-03mAd23q9X4/Wpdf21tcU1I/AAAAAAALNHk/g_f7-GmXrW0CGu84ukzyl2HSo7x_3TLqwCLcBGAs/s1600/AS003723_02.gif"
            , "https://i.pinimg.com/originals/c2/72/78/c272780f3e81f072283f9210e21c460c.jpg"
            , "https://stickerly.pstatic.net/sticker_pack/GWE06IgDSetIKzU3GDJslg/X7AP86/20/cae7fcd9-4443-4fbb-a5fa-a0ba9a125d05.png",
            "https://i.pinimg.com/originals/da/29/ed/da29ede8b37b3169bd94b76e97ba492c.jpg",
            "https://i.pinimg.com/236x/ec/3c/f0/ec3cf0f10360168f982aa9de43d452f5.jpg","https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_02.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_07.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_08.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_19.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_11.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_12.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_13.png");

    public static class MapKey {
        public static final String UNREAD_MESSAGE = "unreadMessage";
    }

    public enum StateUser {
        FRIEND,
        REQUEST_FRIEND,
        OTHERS
    }

    public enum AppLanguage {
        VIETNAMES("vi", "Tiếng Việt"),
        ENGLISH("en", "English");

        String locale, language;

        AppLanguage(String locale, String language) {
            this.locale = locale;
            this.language = language;
        }

        public static String[] getListLanguage() {
            List<String> results = new ArrayList<>();
            for (AppConstants.AppLanguage language : values()) {
                results.add(language.getLanguage());
            }
            return results.toArray(new String[0]);
        }

        public static int getIndexLanguage(String locale) {
            List<AppLanguage> languages = Arrays.asList(values());
            for (AppConstants.AppLanguage language : languages) {
                if (language.locale.equals(locale)) {
                    return languages.indexOf(language);
                }
            }
            return -1;
        }

        public String getLocale() {
            return locale;
        }

        public String getLanguage() {
            return language;
        }
    }

    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
