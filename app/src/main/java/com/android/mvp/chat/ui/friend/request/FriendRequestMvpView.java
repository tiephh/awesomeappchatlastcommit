package com.android.mvp.chat.ui.friend.request;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface FriendRequestMvpView extends MvpView {
    void readSender(ArrayList<Users> users);
    void readReceiver(ArrayList<Users> users);
}
