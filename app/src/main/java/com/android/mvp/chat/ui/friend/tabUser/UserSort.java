package com.android.mvp.chat.ui.friend.tabUser;

import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.utils.AppConstants;

public class UserSort {
    private String header;
    private Users users;
    private boolean isSection;
    private AppConstants.StateUser stateUser;

    public UserSort(String header, Users users, boolean isSection, AppConstants.StateUser stateUser) {
        this.header = header;
        this.users = users;
        this.isSection = isSection;
        this.stateUser = stateUser;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        header = header;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public boolean isSection() {
        return isSection;
    }

    public void setSection(boolean section) {
        isSection = section;
    }

    public AppConstants.StateUser getStateUser() {
        return stateUser;
    }

    public void setStateUser(AppConstants.StateUser stateUser) {
        this.stateUser = stateUser;
    }

    public boolean isFriend() {
        return stateUser.equals(AppConstants.StateUser.FRIEND);
    }

    public boolean isRequestFriend() {
        return stateUser.equals(AppConstants.StateUser.REQUEST_FRIEND);
    }
}
