package com.android.mvp.chat.ui.profile.changePofile;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.android.mvp.chat.data.DataManager;
import com.android.mvp.chat.data.firebase.Users;
import com.android.mvp.chat.ui.base.BasePresenter;
import com.android.mvp.chat.utils.AppConstants;
import com.android.mvp.chat.utils.rx.SchedulerProvider;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ChangeProfilePresenter <V extends ChangeProfileMvpView> extends BasePresenter<V>
        implements ChangeProfileMvpPresenter<V> {


    @Inject
    public ChangeProfilePresenter(DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
        setUpView();
    }

    public void setUpView() {
        getDataManager().getCurrentDataUser()
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Users senderByMe = dataSnapshot.getValue(Users.class);
                        if(senderByMe != null){
                            getMvpView().setUpView(senderByMe);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        getMvpView().showMessage(databaseError.getMessage());
                    }
                });
    }

    @Override
    public void uploadImage(Uri mUri) {
        StorageReference reference = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = reference.child("images/"+mUri.getLastPathSegment());
        riversRef.putFile(mUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                riversRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        getDataManager().getDatabaseReference(AppConstants.USERS)
                                .child(getDataManager().getFirebaseUserId())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        Users users = dataSnapshot.getValue(Users.class);
                                        if ((users != null)){
                                            users.setImageURL(uri.toString());
                                            dataSnapshot.getRef().setValue(users);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                    }
                });
            }
        });

    }

    @Override
    public void uploadName(String name) {
        HashMap<String, Object> map = new HashMap<>();
        map.put(AppConstants.USER_NAME_RES, name);
        getDataManager().getDatabaseReference(AppConstants.USERS)
                .child(getDataManager().getFirebaseUserId())
                .updateChildren(map);
    }
}