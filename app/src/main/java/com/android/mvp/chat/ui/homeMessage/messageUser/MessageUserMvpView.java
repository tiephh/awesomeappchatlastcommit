package com.android.mvp.chat.ui.homeMessage.messageUser;

import android.content.Context;

import com.android.mvp.chat.data.firebase.Message;
import com.android.mvp.chat.ui.base.MvpView;

import java.util.ArrayList;

public interface MessageUserMvpView extends MvpView {
    void setDataMessage(ArrayList<Message> arrayList, String uId);
    Context getContext();
}
