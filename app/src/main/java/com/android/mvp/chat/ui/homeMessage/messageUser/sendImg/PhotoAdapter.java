package com.android.mvp.chat.ui.homeMessage.messageUser.sendImg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.mvp.chat.R;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    private Context mContext;
    private List<String> mListPhoto;
    private ArrayList<String> listImg = new ArrayList<>();
    private int count = 0;

    public PhotoAdapter(Context mContext, List<String> mListPhoto) {
        this.mContext = mContext;
        this.mListPhoto = mListPhoto;
    }

    public interface IOnClick {
        void itemClick(String name);
    }
    public ArrayList<String> getListImg(){
        return listImg;
    }

    public void clearList(){
        listImg.clear();

    }

    public IOnClick iOnClick;

    public void setOnClick(IOnClick iOnClick) {
        this.iOnClick = iOnClick;
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image, viewGroup, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder photoViewHolder, int i) {

        String image = mListPhoto.get(i);
        photoViewHolder.bindView(image, iOnClick);

    }

    @Override
    public int getItemCount() {
        if (mListPhoto == null) {
            return 0;
        } else {
            return mListPhoto.size();
        }
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_gallery)
        ImageView ivGallery;

        @BindView(R.id.tv_click)
        TextView tvClick;

        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bindView(String image, IOnClick iOnClick) {
            Glide.with(mContext).load(new File(image)).into(ivGallery);
            ivGallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (tvClick.getVisibility() == View.VISIBLE) {
                        tvClick.setVisibility(View.GONE); // Lan thu 2 no dang vao day
                    } else {
                        tvClick.setVisibility(View.VISIBLE);
                    }
                    if (listImg.contains(image)){
                        listImg.remove(image);
                    } else {
                        listImg.add(image);
                        tvClick.setText(""+listImg.size());
                    }
                    if(iOnClick != null){
                        iOnClick.itemClick(image);
                    }
                }
            });
        }
    }
}
