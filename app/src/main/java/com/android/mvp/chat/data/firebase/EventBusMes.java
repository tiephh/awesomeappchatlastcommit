package com.android.mvp.chat.data.firebase;

public class EventBusMes {
    int message;

    public EventBusMes(int message) {
        this.message = message;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }
}
